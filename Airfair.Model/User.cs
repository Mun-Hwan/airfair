﻿using System.ComponentModel.DataAnnotations;

namespace Airfair.Model
{
    public class User
    {
        [Key]
        public int idx { get; set; }

        public int code { get; set; }

        public string company_name { get; set; }

        public string n_name { get; set; }

        public string n_email { get; set; }

        public string reg_date { get; set; }
    }
}
