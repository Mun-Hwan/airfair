﻿using Airfair.IDAL;
using Airfair.Model;
using System;
using System.Collections.Generic;

namespace Airfair.UserBLL
{
    public class UserBll
    {
        private readonly IUserDal _userDal;

        public UserBll(IUserDal userDal)
        {
            _userDal = userDal;
        }

        public List<User> GetUserList()
        {
            return _userDal.GetUserList();
        }

        public bool RemoveUser(int? idx)
        {
            if (idx == null)
            {
                return false;
            }

            if (idx <= 0)
            {
                return false;
            }
            return _userDal.RemoveUser(idx);
        }
    }
}
