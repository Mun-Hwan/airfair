﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Airfair.MVC6.Models;
using Airfair.UserBLL;

namespace Airfair.MVC6.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserBll _userBll;

        public HomeController(UserBll userBll)
        {
            _userBll = userBll;
        }

        public IActionResult Index()
        {
            ViewBag.UserList = _userBll.GetUserList();

            return View();
        }

        public IActionResult Remove(int? idx)
        {
            var rtn = _userBll.RemoveUser(idx);

            ViewBag.ReturnFlag = rtn;
            if (rtn == true)
                ViewBag.ReturnMsg = "삭제 되었습니다.";
            else
                ViewBag.ReturnMsg = "실패 하였습니다.";

            return RedirectToAction("Index");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
