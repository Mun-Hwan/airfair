﻿using Airfair.Model;
using System;
using System.Collections.Generic;

namespace Airfair.IDAL
{
    public interface IUserDal
    {
        List<User> GetUserList();

        bool RemoveUser(int? idx);
    }
}
