﻿using Airfair.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Airfair.DAL.DataContext
{
    public class AirfairDbContext : DbContext
    {

        private readonly IConfiguration _configuration;

        public AirfairDbContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public DbSet<User> Airfair_visitor { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("Conn_EimsKorea"));
        }
    }
}
