﻿using Airfair.DAL.DataContext;
using Airfair.IDAL;
using Airfair.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Airfair.DAL
{
    public class UserDal : IUserDal
    {
        private readonly IConfiguration _configuration;

        public UserDal(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public List<User> GetUserList()
        {
            using (var db = new AirfairDbContext(_configuration))
            {
                return db.Airfair_visitor
                    .OrderByDescending(u => u.idx)
                    .ToList();
            }
        }

        public bool RemoveUser(int? idx)
        {
            if (idx == null)
            {
                return false;
            }

            using (var db = new AirfairDbContext(_configuration))
            {
                var user = db.Airfair_visitor.FirstOrDefault(u => u.idx == idx);
                db.Airfair_visitor.Remove(user);

                if (db.SaveChanges() > 0)
                    return true;
            }

            return false;
        }
    }
}
